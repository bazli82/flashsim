/* Copyright 2016, Jian Zhou */

/* FlashSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version. */

/* FlashSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License
 * along with FlashSim.  If not, see <http://www.gnu.org/licenses/>. */

/****************************************************************************/

/* SPC Trace test driver
 * Jian Zhou */

#include "ssd.h"

#define SIZE 130

using namespace ssd;

int main(int argc, char *argv[]) {
	load_config();
	print_config(NULL);
	//printf("Press ENTER to continue...");
	//getchar();
	printf("\n");

	Ssd *ssd = new Ssd;

	unsigned int asu;
	unsigned long lba;
	unsigned int size;
	char opcode;
	enum event_type op;
	double timestamp;
	double result = (double) 0.0;
	unsigned long num = 0;

	FILE *trace = NULL;
	char *filename = argv[1];

	if((trace = fopen(filename, "r")) == NULL){
		printf("File was moved or access was denied.\n");
		exit(-1);
	}

	char line[80];
	while(fgets(line, 80, trace) != NULL){
		sscanf(line, "%u,%lu,%u,%c,%lf", &asu, &lba, &size, &opcode, &timestamp);
		if(opcode == 'W') {
			op = WRITE;
		} else if(opcode == 'R') {
			op = READ;
		}
		num++;
		result += ssd->event_arrive(op, lba, 1, timestamp);
	}
	fclose(trace);

	ssd->print_ftl_statistics();
	printf("average delay: %lf\n", result/num);

	FILE *logFile = NULL;
	if ((logFile = fopen("output.log", "w")) == NULL) {
		printf("Output file cannot be written to.\n");
		exit(-1);
	}
	ssd->write_statistics(logFile);
	fclose(logFile);

  delete ssd;
	return 0;
}
